<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_logoSWAGLabs</name>
   <tag></tag>
   <elementGuidId>4594c3cb-5918-4ff3-adf3-33a9d57f82d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.app_logo</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='header_container']/div/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e86b00f3-beeb-4285-a755-62aa8db4211d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>app_logo</value>
      <webElementGuid>3ed40919-4c0b-4fcb-a0db-8731cefa9895</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header_container&quot;)/div[@class=&quot;primary_header&quot;]/div[@class=&quot;header_label&quot;]/div[@class=&quot;app_logo&quot;]</value>
      <webElementGuid>8eac3aba-dd08-44a8-a78a-8b53d3b365bc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='header_container']/div/div[2]/div</value>
      <webElementGuid>e663289a-6305-4a08-8d34-2a2c175f9ca6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div[2]/div</value>
      <webElementGuid>6db2afa1-9501-4db1-bae6-fa5bc8f4050a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
