<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_menu</name>
   <tag></tag>
   <elementGuidId>6efd5624-84b2-40a2-84e8-661d2144bcd9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-burger-menu-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='react-burger-menu-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fc6e41f1-c709-4934-bc9e-8c49708a12c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-burger-menu-btn</value>
      <webElementGuid>688e6f7f-4249-40cc-aba1-ba210eb6d30f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Open Menu</value>
      <webElementGuid>041a4567-a9eb-4b59-b102-4e16f70131cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-burger-menu-btn&quot;)</value>
      <webElementGuid>f928a888-08aa-401f-9123-4af330561f4b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='react-burger-menu-btn']</value>
      <webElementGuid>7c95ca30-4ca0-4c36-a020-b2f97ff429be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='menu_button_container']/div/div/div/button</value>
      <webElementGuid>92d06c9e-5ea5-41fc-b4cf-cea1a0873bb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About'])[1]/preceding::button[1]</value>
      <webElementGuid>54a328d2-cce7-4dce-9dc3-2275af20e3da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close Menu'])[1]/preceding::button[1]</value>
      <webElementGuid>fe693c6f-656c-4362-98fb-604df1c414da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Open Menu']/parent::*</value>
      <webElementGuid>1d20c2b1-de5c-42f2-b2d2-0dd730ff8317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>0ba814af-fdfd-4d45-9692-96156afe3bff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'react-burger-menu-btn' and (text() = 'Open Menu' or . = 'Open Menu')]</value>
      <webElementGuid>1310f16a-315b-4c28-bc2b-47700aec5f21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
