import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('stepDefinition/featureLogin/LGI005 - User want to login using correct credential'), [('errorMessage') : 'Epic sadface: Username and password do not match any user in this service'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('pages/pageProduct/verifyContent'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToElement(findTestObject('pageProduct/img_2023Sauce Labs. All Rights Reserved. Terms of Service  Privacy Policy_footer_robot'), 
    0)

WebUI.verifyElementVisible(findTestObject('pageProduct/img_2023Sauce Labs. All Rights Reserved. Terms of Service  Privacy Policy_footer_robot'))

WebUI.scrollToElement(findTestObject('pageProduct/a_shoppingCartLink'), 0)

WebUI.verifyElementVisible(findTestObject('pageProduct/a_shoppingCartLink'))

